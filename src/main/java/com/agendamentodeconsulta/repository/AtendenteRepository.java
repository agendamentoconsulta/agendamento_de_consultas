package com.agendamentodeconsulta.repository;

import com.agendamentodeconsulta.model.Atendente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AtendenteRepository extends JpaRepository <Atendente, Long>{

    List<Atendente> findByNome(String nome);

    Atendente findOneByEmail(String email);

    @Query("select u from Atendente u where u.email like :email")
    Atendente findByEmail(@Param("email") String email);

}
