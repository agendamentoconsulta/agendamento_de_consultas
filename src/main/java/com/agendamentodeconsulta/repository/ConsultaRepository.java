package com.agendamentodeconsulta.repository;

import com.agendamentodeconsulta.model.Consulta;
import com.agendamentodeconsulta.model.Horario;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface ConsultaRepository extends JpaRepository<Consulta, Long> {

    @Query(value = "SELECT new com.agendamentodeconsulta.dto.ConsultaDTO(c.id, c.dataConsulta, c.horario.horaMinuto, " +
            "c.paciente.nome, c.status, c.medico.nome, c.medico.especialidade.nome) " +
            "FROM Consulta as c")
    Page<Consulta> listAll(Pageable pageable);

    List<Consulta> findByDataConsulta(LocalDateTime data);

    List<Consulta> findConsultaByDataConsultaBetween(LocalDate dInicio, LocalDate dFinal);

    @Query("select h "
            + "from Horario h "
            + "where not exists("
            + "select a.horario.id "
            + "from Consulta a "
            + "where "
            + "a.medico.id = :id and "
            + "a.dataConsulta = :data and "
            + "a.horario.id = h.id "
            + ") "
            + "order by h.horaMinuto asc")
    List<Horario> findByMedicoIdAndDataNotHorarioAgendado(Long id, LocalDate data);
}
