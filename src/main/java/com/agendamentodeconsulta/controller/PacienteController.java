package com.agendamentodeconsulta.controller;

import com.agendamentodeconsulta.model.Paciente;
import com.agendamentodeconsulta.repository.PacienteRepository;
import com.agendamentodeconsulta.service.PacienteService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("pacientes")
public class PacienteController extends DefaultController<Paciente, PacienteService>{

    @GetMapping
    public ResponseEntity<Page<Paciente>> listAll(Pageable page) {
        return ResponseEntity.ok(getService().listAll(page));
    }
}

