package com.agendamentodeconsulta.controller;

import com.agendamentodeconsulta.model.Autocomplete;
import com.agendamentodeconsulta.model.Medico;
import com.agendamentodeconsulta.repository.MedicoRepository;
import com.agendamentodeconsulta.service.MedicoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("medicos")
public class MedicoController extends DefaultController<Medico, MedicoService> {


    @GetMapping
    public ResponseEntity<Page<Medico>> listAll(Pageable pageable){
        return ResponseEntity.ok(getService().listAll(pageable));
    }

    @GetMapping("/{id}/especialidade")
    public ResponseEntity<List<Medico>> ListarMedicsPorEspecialidade(@PathVariable Long id) {

        return ResponseEntity.ok(getService().findMedicosByEspecialidade(id));
    }

    @GetMapping("/autocomplete")
    public ResponseEntity<Page<Autocomplete>> autocomplete(String date, String search, Pageable pageable) {
        LocalDateTime parseDate = LocalDateTime.parse(date);
        return ResponseEntity.ok(getService().autocomplete(parseDate, search, pageable));
    }

}


