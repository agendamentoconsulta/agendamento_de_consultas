package com.agendamentodeconsulta.controller;

import com.agendamentodeconsulta.model.Consulta;
import com.agendamentodeconsulta.model.Horario;
import com.agendamentodeconsulta.service.ConsultaService;
import net.minidev.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("consultas")
public class ConsultaController extends DefaultController<Consulta, ConsultaService> {

    private final ConsultaService consultaService;

    public ConsultaController(ConsultaService consultaService) {
        this.consultaService = consultaService;
    }

    @GetMapping
    public ResponseEntity<Page<Consulta>> listAll(Pageable pageable) {
        return ResponseEntity.ok(consultaService.listAll(pageable));
    }

    @PostMapping("iniciar/{id}")
    public void iniciarConsulta(@PathVariable Long id) {
        getService().iniciarConsulta(id);
    }

    @PostMapping("concluir/{id}")
    public void concluirContaulta(@PathVariable Long id) {
        getService().concluirConsulta(id);
    }

    @GetMapping("/horario/medico/{id}/data/{data}")
    public ResponseEntity<?> horariosPorMededicos(@PathVariable("id") Long id,
                                                  @PathVariable("data") String data) {

        return ResponseEntity.ok(consultaService.buscarHorariosNaoAgendadosPorMedicoIdEData(id, data));
    }

    @GetMapping("/horarios")
    public ResponseEntity<List<Horario>> horarioListAll() {
        return ResponseEntity.ok(consultaService.horarioListAll());
    }
}
