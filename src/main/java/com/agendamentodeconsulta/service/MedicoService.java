package com.agendamentodeconsulta.service;

import com.agendamentodeconsulta.model.Autocomplete;
import com.agendamentodeconsulta.model.Medico;
import com.agendamentodeconsulta.repository.MedicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class MedicoService extends DefaultService<Medico, MedicoRepository> {

    @Autowired
    ConsultaService consultaService;

    public Page<Medico> listAll(Pageable pageable){
        return getRepository().listAll(pageable);
    }

    @Override
    public List<Medico> procuraPorString(String search) {
        return getRepository().findAllByNomeContaining(search);    }


    public List<Medico> findMedicosByEspecialidade(Long id) {
       return getRepository().findAllByEspecialidade_Id(id);
    }

    public Page<Autocomplete> autocomplete(LocalDateTime date, String search, Pageable pageable) {
        return getRepository().autocompleteMedicosPorDisponibilidadeDeHorario(date, search, pageable);
    }



}
