package com.agendamentodeconsulta.service;

import com.agendamentodeconsulta.model.Atendente;
import com.agendamentodeconsulta.repository.AtendenteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AtendenteService implements UserDetailsService {

    private final AtendenteRepository atendenteRepository;

    private final PasswordEncoder encoder;

    @Autowired
    public AtendenteService(AtendenteRepository atendenteRepository, PasswordEncoder encoder) {

        this.atendenteRepository = atendenteRepository;
        this.encoder = encoder;
    }

    public Page<Atendente> listAll(Pageable pageable) {
        return atendenteRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Atendente buscarPorEmail(String email) {

        return atendenteRepository.findByEmail(email);
    }

    @Override @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Atendente usuario = buscarPorEmail(username);
        return new User(
                usuario.getEmail(),
                usuario.getSenha(),
                AuthorityUtils.createAuthorityList(username)
        );
    }

    @Transactional(readOnly = false)
    public Atendente adicionarAtendente(Atendente atendente) {

        String encodedPassword = encoder.encode(atendente.getSenha());
        atendente.setSenha(encodedPassword);

        return atendenteRepository.save(atendente);
    }

    public List<Atendente> findByNome(String nome) {

        return atendenteRepository.findByNome(nome);
    }

    public Atendente atualizarAtendente(Atendente atendente) {

       return atendenteRepository.save(atendente);
    }

    public void excluir(Long id) {

        atendenteRepository.deleteById(id);
    }

    public Atendente getUser(String username) {

        return atendenteRepository.findOneByEmail(username);
    }
}
