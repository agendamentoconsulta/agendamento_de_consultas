package com.agendamentodeconsulta.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MedicoDTO {

    private Long id;

    private String nome;

    private String especialidade;

}
