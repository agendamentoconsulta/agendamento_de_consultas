package com.agendamentodeconsulta.dto;

import com.agendamentodeconsulta.model.StatusConsulta;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ConsultaDTO {

    private Long id;

    private LocalDate dataConsulta;

    private LocalTime horario;

    private String paciente;

    private StatusConsulta status;

    private String medico;

    private String especialidade;
}
